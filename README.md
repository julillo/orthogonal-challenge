# orthogonal-challenge

## Introduction

- Hi, this is the solution of the Orthogonal exam submitted by Julio Sánchez
- To run this solution, you should have installed node and .NET SDK on your machine

## Content and instructions

**task-1**: Contains the .NET App written using React.js
1. Run "npm install" inside ClientApp folder (cd task-1/ClientApp) and wait while the packages get installed
2. Run "dotnet run" inside task-1 folder (cd ..)
3. From the output, copy the url starting with "https://localhost:..." and paste it on your browser
4. (In case a "not secure page" warning appears due to https, please proceed to open the url anyway)
5. You should be redirect to the form

**task-2**: Contains 2 files:
- The SQL code was tested using www.db-fiddle.com and PostgreSQL v11 database
1. scheme-script.sql: Should be run to create and poblate the tables.
2. queries.sql: Should be run to perform que requested queries.
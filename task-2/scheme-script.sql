CREATE TABLE "case" (
  id INT,
  case_owner_name VARCHAR,
  service_date DATE,
  completed_date DATE,
  creation_date DATE NOT NULL DEFAULT CURRENT_DATE, --creation of the record
  last_update_date DATE NOT NULL DEFAULT CURRENT_DATE, --last update of the record
  CONSTRAINT cases_pk PRIMARY KEY (id)
);

INSERT INTO
  "case" (id, case_owner_name, service_date, completed_date)
VALUES
  (1, 'Tom', '2022-01-01', '2022-02-13');

INSERT INTO
  "case" (id, case_owner_name, service_date)
VALUES
  (2, 'Jerry', '2022-02-01');

CREATE TABLE attachment (
  id INT,
  case_id INT NOT NULL,
  name VARCHAR,
  filesize integer,
  storage_path VARCHAR,
  creation_date DATE NOT NULL DEFAULT CURRENT_DATE, --creation of the record
  last_update_date DATE NOT NULL DEFAULT CURRENT_DATE, --last update of the record
  CONSTRAINT attachment_pk PRIMARY KEY (id),
  CONSTRAINT attachment_fk FOREIGN KEY (case_id) REFERENCES "case"(id)
);

INSERT INTO
  attachment (id, case_id, name, filesize, storage_path)
VALUES
  (1, 1, 'att1', 2147483647, 'att1_url');

INSERT INTO
  attachment (id, case_id, name)
VALUES
  (2, 2, 'att2');

CREATE TABLE job_number (
  id INT,
  case_id INT NOT NULL,
  job_number bigint NOT NULL,
  partner_id INT,
  creation_date DATE NOT NULL DEFAULT CURRENT_DATE,
  last_update_date DATE NOT NULL DEFAULT CURRENT_DATE,
  CONSTRAINT job_number_pk PRIMARY KEY (id),
  CONSTRAINT job_number_fk FOREIGN KEY (case_id) REFERENCES "case"(id)
);

INSERT INTO
  job_number (id, case_id, job_number, partner_id)
VALUES
  (1, 1, 100000123, 10);
  INSERT INTO
  job_number (id, case_id, job_number, partner_id)
VALUES
  (2, 2, 100000124, 10);
  INSERT INTO
  job_number (id, case_id, job_number, partner_id)
VALUES
  (3, 2, 100000125, 10);
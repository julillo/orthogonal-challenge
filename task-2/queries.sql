-- 1) list of all cases and attachment filenames. I added a couple of fields to the query that could be helpful
SELECT c.id, c.case_owner_name as "owner_name", to_char(c.service_date, 'DD-MM-YYYY') as "service_date", to_char(c.creation_date, 'DD-MM-YYYY') as "creation_date", a.name as "attachment_name", a.storage_path FROM "case" c
JOIN attachment a ON a.case_id = c.id;

-- 2) list of all cases and job numbers. I added a couple of fields to the query that could be helpful
SELECT c.id, c.case_owner_name as "owner_name", to_char(c.service_date, 'DD-MM-YYYY') as "service_date", to_char(c.creation_date, 'DD-MM-YYYY') as "creation_date", jn.job_number, jn.partner_id FROM "case" c JOIN job_number jn ON jn.case_id = c.id;


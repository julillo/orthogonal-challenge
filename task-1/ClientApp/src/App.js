import React, { Component } from 'react';
import { Route, Routes } from 'react-router-dom';
import { Layout } from './components/Layout';
import './custom.css';
import Home from './components/Home';

export default function App() {
  const displayName = App.name;
    return (
      <Layout>
        <Home/>
        {/* <Routes>
          {AppRoutes.map((route, index) => {
            const { element, ...rest } = route;
            return <Route key={index} {...rest} element={element} />;
          })}
        </Routes> */}
      </Layout>
    );
}

import React, { useState } from "react";
import {
  FormControl,
  FormLabel,
  TextField,
  MenuItem,
  Select,
  Button,
} from "@mui/material";
import { convertToRaw } from "draft-js";
import { Grid } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import MUIRichTextEditor from "mui-rte";
//Datepicker
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { DateTime } from "luxon";

const myTheme = createTheme({
  // Set up your custom MUI theme here
});

const Home = (props) => {
  //short text const
  const [shortText, setShortText] = useState("");
  //long text consts
  const [richText, setRichText] = useState("");
  const [plainText, setPlainText] = useState("");
  //Date value consts and let
  let today = new Date();
  const tomorrow = today.setDate(today.getDate() + 1);
  // const [dateValue, setDateValue] = useState(DateTime.fromJSDate(tomorrow));
  const [dateValue, setDateValue] = useState(tomorrow);
  const [formattedDate, setFormattedDate] = useState(`${today.getMonth()+1}/${today.getDate()}/${today.getFullYear()}`);
  
  //Select value consts
  const [USAStates] = useState([
    "Select one state",
    "Alabama",
    "Alaska",
    "Arizona",
    "Arkansas",
    "California",
    "Colorado",
    "Connecticut",
    "Delaware",
    "Florida",
    "Georgia",
    "Hawaii",
    "Idaho",
    "Illinois",
    "Indiana",
    "Iowa",
    "Kansas",
    "Kentucky",
    "Louisiana",
    "Maine",
    "Maryland",
    "Massachusetts",
    "Michigan",
    "Minnesota",
    "Mississippi",
    "Missouri",
    "Montana",
    "Nebraska",
    "Nevada",
    "New Hampshire",
    "New Jersey",
    "New Mexico",
    "New York",
    "North Carolina",
    "North Dakota",
    "Ohio",
    "Oklahoma",
    "Oregon",
    "Pennsylvania",
    "Rhode Island",
    "South Carolina",
    "South Dakota",
    "Tennessee",
    "Texas",
    "Utah",
    "Vermont",
    "Virginia",
    "Washington",
    "West Virginia",
    "Wisconsin",
    "Wyoming",
  ]);
  const [selectValue, setSelectValue] = useState("Select one state");

  //validations and values summary const
  const [errors, setErrors] = useState([]);
  const [messages, setMessages] = useState([]);
  const [formSubmited, setFormSubmited] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    setFormSubmited(true);
    let errors = [];
    if (plainText.length == 0) errors.push("Long text is required");
    if (shortText.length == 0) errors.push("Short text is required");
    if (selectValue == USAStates[0]) errors.push("State is required");
    setErrors(errors);

    let messages = [];
    messages.push(`Long text: ${plainText}`)
    messages.push(`Short text: ${shortText}`)
    messages.push(`Date: ${formattedDate}`)
    // messages.push(`Date: ${dateValue}`)
    messages.push(`Select value: ${selectValue}`)
    setMessages(messages)
  };

  const onEditorChange = (event) => {
    // console.log("onEditorChange");
    const plainText = event.getCurrentContent().getPlainText(); // for plain text
    const rteContent = convertToRaw(event.getCurrentContent()); // for rte content with text formating
    setRichText(JSON.stringify(rteContent)); // store your rteContent to state
    setPlainText(plainText);
  };

  return (
    <>
      <div>
        <h1>Hello, guys!</h1>
        <p>Welcome to the task 1 by Julio Sánchez:</p>
      </div>
      <div>
        <form onSubmit={handleSubmit}>
          <LocalizationProvider dateAdapter={AdapterMoment}>
            <ThemeProvider theme={myTheme}>
              <Grid
                container
                alignItems="center"
                justify="center"
                direction="column"
              >
                <Grid item>
                  <FormLabel>Long Text *</FormLabel>
                  <MUIRichTextEditor
                    label="Start typing..."
                    maxLength={2000}
                    // value={richText}
                    name="longText"
                    onChange={(event) => {
                      onEditorChange(event);
                    }}
                    // onChange={(event) => onEditorChange(event)}
                    required
                  />
                </Grid>
                <br />
                <br />
                <Grid item>
                  <TextField
                    id="shortText"
                    name="shortText"
                    label="Short Text"
                    type="text"
                    value={shortText}
                    onChange={(val) => setShortText(val.target.value)}
                    // required
                    inputProps={{ maxLength: 100 }}
                  />
                </Grid>
                <br />
                <br />
                <Grid item>
                  <FormLabel>Date:</FormLabel>
                  <DatePicker
                    // disablePast={true}
                    name="date"
                    minDate={tomorrow}
                    inputFormat="MM/DD/yyyy"
                    value={dateValue}
                    onChange={(newValue) => {
                      setDateValue(newValue);
                      setFormattedDate(newValue.format("MM/DD/YY"))
                    }}
                    renderInput={(params) => (
                      <TextField sx={{ display: "block" }} {...params} />
                    )}
                  />
                </Grid>
                <br />
                <br />
                <Grid item>
                  <FormControl>
                    <FormLabel>Select:</FormLabel>
                    <Select
                      name="USAstate"
                      value={selectValue}
                      onChange={(val) => {
                        setSelectValue(val.target.value);
                      }}
                    >
                      {USAStates.map((state, i) => {
                        return (
                          <MenuItem key={i} value={state}>
                            {state}
                          </MenuItem>
                        );
                      })}
                    </Select>
                  </FormControl>
                </Grid>
                <br />
                <br />
                <Grid item>
                  <Button variant="contained" color="primary" type="submit">
                    Save
                  </Button>
                </Grid>
              </Grid>
            </ThemeProvider>
          </LocalizationProvider>
        </form>
        {formSubmited ? (
          errors.length > 0 ? (
            <>
              <p>Validations: </p>
              <ul>
                {errors.map((err, i) => (
                  <li style={{ color: "red" }} key={i}>
                    {err}
                  </li>
                ))}
              </ul>
            </>
          ) : (
            <>
            <p>Values entered: </p>
            <ul>
                {messages.map((msg, i) => (
                  <li key={i}>
                    {msg}
                  </li>
                ))}
              </ul></>
          )
        ) : null}
      </div>
    </>
  );
};

export default Home;
